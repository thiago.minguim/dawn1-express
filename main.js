const express = require("express");
const app = express();
app.listen(3000);

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/views/index.html");
});
app.get("/sobre", function (req, res) {
  res.sendFile(__dirname + "/views/sobre.html");
});
app.get("/contato", function (req, res) {
  res.sendFile(__dirname + "/views/contato.html");
});

app.use(express.urlencoded({ extended: true }));
app.post("/confirmacao", (req, res) => {
  console.log(req.body.nome);
  console.log(req.body.email);
  console.log(req.body.text);

  const { nome, email, text } = req.body;
  res.send(
    `Obrigado ${nome}, por ter nos enviado  o texto ${text} retornaremos pelo email ${email}`
  );
});
